from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

from datetime import datetime, timedelta
import iso8601


try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

SCOPES = 'https://www.googleapis.com/auth/calendar'
CLIENT_SECRET_FILE = '/media/ryan/Data/Code/dpSite/dp.API/helpers/client_secret.json'
APPLICATION_NAME = 'DataPoint GIS Appointment Maker'

class MakeAppointment():

    def get_credentials(self):
        base_dir = os.path.expanduser('/media/ryan/Data/Code/dpSite/dp.API/helpers/')
        credential_dir = os.path.join(base_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir,
                                    'calendar-python-quickstart.json')

        store = Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
            flow.user_agent = APPLICATION_NAME
            if flags:
                credentials = tools.run_flow(flow, store, flags)
            print('Storing credentials to ' + credential_path)
        return credentials


    def create_event(self, appointment):
        return {'summary': 'Initial Phone Consultation with DataPoint GIS',
                'location': 'Phone',
                'description': 'Phone call to discover possbilities.',
                'start': {'dateTime': appointment.appointment.isoformat()},
                'end': {'dateTime': (appointment.appointment + timedelta(seconds=1800)).isoformat()},
                'attendees': [
                    {'email': 'bertramdryan@gmail.com'},
                    {'email': appointment.email}
                ],
                'reminders': {
                        'useDefault': False,
                        'overrides': [
                        {'method': 'email', 'minutes': 24 * 60},
                        {'method': 'popup', 'minutes': 10},
                        ],
                    },
        }


    def save_event(self, appointment):
        credentials = self.get_credentials()
        http = credentials.authorize(httplib2.Http())
        service = discovery.build('calendar', 'v3', http=http)
        event = self.create_event(appointment)
        event = service.events().insert(calendarId='primary', body=event).execute()
        return event['id']

