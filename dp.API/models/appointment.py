from db import db

class AppointmentModel(db.Model):
    __tablename__ = 'appointments'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    organization_name = db.Column(db.String(200), nullable=True)
    email = db.Column(db.String(120), nullable=False)
    phone = db.Column(db.String(20), nullable=True)
    appointment = db.Column(db.DateTime, nullable=False)
    google_appointment_id = db.Column(db.String(200), nullable=False)


    def __init__(self, name, organization_name, email, phone, appointment, google_appointment_id = ''):
        self.name = name
        self.organization_name = organization_name
        self.email = email
        self.phone = phone
        self.appointment = appointment
        self.google_appointment_id = google_appointment_id


    def json(self):
        return {
            'name': self.name,
            'organization_name': self.organization_name,
            'email': self.email,
            'phone': self.phone,
            'appointment': self.appointment.isoformat(),
        }

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

