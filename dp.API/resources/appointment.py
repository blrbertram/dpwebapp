import iso8601
from flask_restful import Resource, reqparse
from models.appointment import AppointmentModel
from helpers.google_cal import MakeAppointment



class Appointment(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument(
        'name',
        type=str,
        required = True,
        help="contact name is required."
    )

    parser.add_argument(
        'organization_name',
        type=str,
        required=False,
        help="What organization do you represent."
    )
    parser.add_argument(
        'email',
        type=str,
        required=True,
        help="email is required."
    )

    parser.add_argument(
        'phone',
        type=str,
        required=True,
        help="Please include a contact phone number."
    )

    parser.add_argument(
        'appointment',
        type=str,
        required=True,
        help="Please add date and time for appointment."
    )


    def post(self):
        data = Appointment.parser.parse_args()
        makeAppointment = MakeAppointment()
        appointment = AppointmentModel(name=data['name'],
                                       organization_name=data['organization_name'],
                                       email=data['email'],
                                       phone=data['phone'],
                                       appointment=iso8601.parse_date(data['appointment']))
        
        try:     
            appointment.google_appointment_id = makeAppointment.save_event(appointment)
            appointment.save_to_db()
        except Exception as e:
            return {'message': str(e)}, 500
        
        return appointment.json(), 201

