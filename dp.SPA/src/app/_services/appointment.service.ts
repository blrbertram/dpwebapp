import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http'
import { Appointment } from '../_models/appointment.model'
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment.prod';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@Injectable()
export class AppointmentService {
    rootUrl = environment.rootUrl;

    constructor(private http: HttpClient) {}

    createAppointment(appointment: Appointment) {
        return this.http.post(this.rootUrl + 'appointment', appointment);
    }

    private handleError(error: any) {
        const applicationError = error.headers.get('Application-Error') ;
        if (applicationError) {
            return Observable.throw(applicationError);
        }

        const serverError = error.json();
        let modelStateErrors = '';
        if (serverError) {
            for (const key in serverError) {
                if (serverError[key]) {
                    modelStateErrors += serverError[key] + '\n';
                }
            }
        }
        return Observable.throw(
            modelStateErrors || 'Server Error'
        );
    }


}
