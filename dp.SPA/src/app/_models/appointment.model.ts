export interface Appointment {
    name: string;
    organization: string;
    phone: string;
    email: string;
    appointment: Date;
}