import { Component, OnInit } from '@angular/core';

import { Appointment } from '../_models/appointment.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppointmentService } from '../_services/appointment.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router } from '@angular/router';


@Component({
    selector: 'app-appointment',
    templateUrl: './appointment.component.html',
    styleUrls: ['./appointment.component.css']
})

export class AppointmentComponent implements OnInit {
    appointment: Appointment;
    appointmentForm: FormGroup;


    constructor(private _appointment: AppointmentService,
                private _alertify: AlertifyService,
                private _fb: FormBuilder,
               ) {}

    ngOnInit() {
        this.createAppointmentForm();
    }

    createAppointmentForm() {
        this.appointmentForm = this._fb.group(
            {
                name: ['', Validators.required],
                organization: ['', Validators.max(100)],
                email: ['', [
                             Validators.required,
                             Validators.email
                            ]
                        ],
                phone: ['', Validators.required],
                appointment: [null, Validators.required]
            }
        );
    }

    onAppointmentSubmit() {
        if (this.appointmentForm.valid) {
            this.appointment = Object.assign({}, this.appointmentForm.value);
            this._appointment.createAppointment(this.appointment).subscribe(() => {
                this._alertify.success('Appointment Set');
            }, error =>  {
                this._alertify.error('Something went wrong, try again later.');
            }, () => {
               console.log('made it');
            });
        }
    }
}
